package org.nwolfhub.vkUser;

import com.google.gson.JsonParser;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.apache.http.client.utils.URIBuilder;
import org.nwolfhub.vkUser.longpoll.LongPoll;
import org.nwolfhub.vk.requests.Request;

import java.io.IOException;
import java.net.URISyntaxException;

public class Vk {

    private String token;
    public Boolean success;
    public String username;
    public Integer id;
    public LongPoll lp;
    public OkHttpClient client;
    public String v;

    public String makeRequest(Request r) throws IOException {
        try {
            Response res = client.newCall(new okhttp3.Request.Builder().url(new URIBuilder(r.toString()).addParameter("access_token", token).addParameter("v", v).toString()).build()).execute();
            String toReturn = res.body().string();
            res.close();
            return toReturn;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return "";
        }
    }

    public Vk(String token) throws IOException {
        this.token = token;
        this.client = new OkHttpClient();
        this.v = "5.131";
        String userget = makeRequest(new Request("users.get", "fields=domain"));
        if(JsonParser.parseString(userget).getAsJsonObject().get("error")!=null) throw new IOException("Failed to make users.get request. Is your token correct?");
        this.username = JsonParser.parseString(userget).getAsJsonObject().get("response").getAsJsonArray().get(0).getAsJsonObject().get("domain").getAsString();
        this.id = JsonParser.parseString(userget).getAsJsonObject().get("response").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsInt();
    }
    public LongPoll initializeLongPoll() throws IOException {
        try {
            this.lp = new LongPoll(this);
            return this.lp;
        } catch (IOException e) {
            e.printStackTrace();
            this.lp = null;
            throw new IOException("Failed to init LongPoll. Try again using Vk.initializeLongPoll()");
        }
    }

    public String getToken() {
        return token;
    }

    public Vk setToken(String token) {
        this.token = token;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Vk setUsername(String username) {
        this.username = username;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public Vk setId(int id) {
        this.id = id;
        return this;
    }

    public LongPoll getLp() {
        return lp;
    }

    public Vk setLp(LongPoll lp) {
        this.lp = lp;
        return this;
    }

    public String getV() {
        return v;
    }

    public Vk setV(String v) {
        this.v = v;
        return this;
    }
}
