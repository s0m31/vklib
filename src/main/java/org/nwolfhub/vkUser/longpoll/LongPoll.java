package org.nwolfhub.vkUser.longpoll;

import com.google.gson.*;
import okhttp3.Response;
import org.nwolfhub.vkUser.Vk;
import org.nwolfhub.vkUser.longpoll.updates.Update;
import org.nwolfhub.vkUser.longpoll.updates.UpdateProcessor;
import org.nwolfhub.vk.requests.Request;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class LongPoll {
    private Vk vk;
    private String key;
    public Long ts;
    public String server;
    public Integer wait;

    public LongPoll(Vk vk) throws IOException {
        String r = vk.makeRequest(new Request("messages.getLongPollServer", "need_pts=0"));
        this.wait = 25;
        this.vk = vk;
        try {
            JsonObject jo = JsonParser.parseString(r).getAsJsonObject().get("response").getAsJsonObject();
            //System.out.println(r);
            if(jo.get("failed")==null) {
                key = jo.get("key").getAsString();
                ts = jo.get("ts").getAsLong();
                server = "https://" + jo.get("server").getAsString();
            } else {
                throw new IOException("Failed with code " + jo.get("failed").getAsInt());
            }
        } catch (JsonParseException e) {
            System.out.println("Failed to obtain LongPoll connection data. Object not initialized!");
            key = null;
            ts = null;
            server = null;
            throw new IOException();
        }
    }

    private Response getUpdatesRequest() throws IOException {
        try {
            return (this.vk.client.newCall(new okhttp3.Request.Builder().url(server + "?act=a_check&key=" + key + "&ts=" + ts + "&wait=" + wait + "&mode=2&version=3").build()).execute());
        } catch (SocketTimeoutException e) {
            return null;
        }
    }

    public List<Update> getUpdates(Long ts) throws IOException {
        List<Update> updates = new ArrayList<>();
        this.ts = ts;
        Response resp = getUpdatesRequest();
        if (resp != null) {
            JsonObject getUpdates = JsonParser.parseString(resp.body().string()).getAsJsonObject();
            if(getUpdates.get("failed")==null) {
                this.ts = (long) getUpdates.get("ts").getAsInt();
                JsonArray jArr = getUpdates.getAsJsonArray("updates");
                for (JsonElement updateArr : jArr) {
                    updates.add(UpdateProcessor.convertUpdateByType(updateArr.toString()));
                }
                return updates;
            } else {
                String r = vk.makeRequest(new Request("messages.getLongPollServer", "need_pts=0"));
                JsonObject jo = JsonParser.parseString(r).getAsJsonObject().get("response").getAsJsonObject();
                if(jo.get("failed")==null) {
                    key = jo.get("key").getAsString();
                    ts = jo.get("ts").getAsLong();
                    server = "https://" + jo.get("server").getAsString();
                } else {
                    throw new IOException("Failed with code " + jo.get("failed").getAsInt());
                }
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
    public List<Update> getUpdates() throws IOException {
        List<Update> updates = new ArrayList<>();
        Response resp = getUpdatesRequest();
        if (resp != null) {
            JsonObject getUpdates = JsonParser.parseString(resp.body().string()).getAsJsonObject();
            if(getUpdates.get("failed")==null) {
                this.ts = (long) getUpdates.get("ts").getAsInt();
                JsonArray jArr = getUpdates.getAsJsonArray("updates");
                for (JsonElement updateArr : jArr) {
                    updates.add(UpdateProcessor.convertUpdateByType(updateArr.toString()));
                }
                return updates;
            } else {
                String r = vk.makeRequest(new Request("messages.getLongPollServer", "need_pts=0"));
                JsonObject jo = JsonParser.parseString(r).getAsJsonObject().get("response").getAsJsonObject();
                if(jo.get("failed")==null) {
                    key = jo.get("key").getAsString();
                    ts = jo.get("ts").getAsLong();
                    server = "https://" + jo.get("server").getAsString();
                } else {
                    throw new IOException("Failed with code " + jo.get("failed").getAsInt());
                }
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }

    public List<Update> getNonetypeUpdates() throws IOException {
        List<Update> updates = new ArrayList<>();
        JsonObject getUpdates = JsonParser.parseString(getUpdatesRequest().body().string()).getAsJsonObject();
        this.ts = (long) getUpdates.get("ts").getAsInt();
        JsonArray jArr = getUpdates.getAsJsonArray("updates");
        for(JsonElement updateArr:jArr) {
            updates.add(new Update(updateArr.toString(), "nonetype"));
        }
        return updates;
    }

    public List<Update> getUpdatesByType(String type) throws IOException {
        List<Update> updates = getUpdates();
        List<Update> toReturn = new ArrayList<>();
        for(Update u:updates) {
            if(u.type.equals(type)) toReturn.add(u);
        }
        return toReturn;
    }
    public List<Update> getUpdatesByType(String type, Long ts) throws IOException {
        List<Update> updates = getUpdates(ts);
        List<Update> toReturn = new ArrayList<>();
        for(Update u:updates) {
            if(u.type.equals(type)) toReturn.add(u);
        }
        return toReturn;
    }
    public List<Update> getUpdatesByType(Type type, Long ts) throws IOException {
        List<Update> updates = getUpdates(ts);
        List<Update> toReturn = new ArrayList<>();
        for(Update u:updates) {
            if(u.type.equals(type.toString())) toReturn.add(u);
        }
        return toReturn;
    }
    public enum Type {
        message,
        nonetype
    }

}
