package org.nwolfhub.vkUser.longpoll.updates;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;

public class NewMessageUpdate extends Update{
    public Integer getFrom_id() {
        return from_id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getText() {
        return text;
    }

    public enum From {
        chat,
        user
    }

    public Long message_id;
    public Integer peer_id;
    public Long timestamp;
    public String text;
    public Integer from_id;
    public JsonObject attachments;
    public From from;
    public boolean from_me;

    public NewMessageUpdate(String unparsedUpdate) throws IOException {
        super(unparsedUpdate, "message");
        if(this.parsedUpdate.get(0).getAsInt() == 4) {
            this.message_id = (long) this.getParsedUpdate().get(1).getAsInt();
            if(this.getParsedUpdate().get(3).getAsInt()>2000000000) {
                this.from = From.chat;
                this.peer_id = this.getParsedUpdate().get(3).getAsInt();
                this.from_id = this.getParsedUpdate().get(6).getAsJsonObject().get("from").getAsInt();
                this.text = this.getParsedUpdate().get(5).getAsString();
                this.timestamp = this.getParsedUpdate().get(4).getAsLong();
                this.from_me = ((getParsedUpdate().get(2).getAsInt() & (1 << (2 - 1))) > 0);
                this.attachments = getParsedUpdate().get(7).getAsJsonObject();
            } else {
                this.from = From.user;
                this.from_me = ((getParsedUpdate().get(2).getAsInt() & (1 << (2 - 1))) > 0); //I actually don't understand this line, but I hope it works
                this.peer_id = getParsedUpdate().get(3).getAsInt();
                this.from_id = from_me?0:peer_id;
                this.text = getParsedUpdate().get(5).getAsString();
                this.attachments = getParsedUpdate().get(6).getAsJsonObject();
            }
        } else {
            throw new IOException("Update is not a message");
        }
    }

    public NewMessageUpdate(Update update) throws IOException {
        super(update.unparsedUpdate, "message");
        if(this.parsedUpdate.get(0).getAsInt() == 4) {
            this.message_id = (long) this.getParsedUpdate().get(1).getAsInt();
            if(this.getParsedUpdate().get(3).getAsInt()>2000000000) {
                this.from = From.chat;
                this.peer_id = this.getParsedUpdate().get(3).getAsInt();
                this.from_id = this.getParsedUpdate().get(6).getAsJsonObject().get("from").getAsInt();
                this.text = this.getParsedUpdate().get(5).getAsString();
                this.timestamp = this.getParsedUpdate().get(4).getAsLong();
                this.from_me = ((getParsedUpdate().get(2).getAsInt() & (1 << (2 - 1))) > 0);
                this.attachments = getParsedUpdate().get(7).getAsJsonObject();
            } else {
                this.from = From.user;
                this.from_me = ((getParsedUpdate().get(2).getAsInt() & (1 << (2 - 1))) > 0); //I actually don't understand this line, but I hope it works
                this.peer_id = getParsedUpdate().get(3).getAsInt();
                this.from_id = from_me?0:peer_id;
                this.text = getParsedUpdate().get(5).getAsString();
                this.attachments = getParsedUpdate().get(6).getAsJsonObject();
            }
        } else {
            throw new IOException("Update is not a message");
        }
    }
}
