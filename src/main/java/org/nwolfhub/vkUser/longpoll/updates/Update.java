package org.nwolfhub.vkUser.longpoll.updates;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

public class Update {

    public final String type;
    public String getUnparsedUpdate() {
        return unparsedUpdate;
    }

    public JsonArray getParsedUpdate() {
        return parsedUpdate;
    }

    public final String unparsedUpdate;
    public final JsonArray parsedUpdate;

    public Update(String unparsedUpdate, String type) {
        this.type = type;
        this.unparsedUpdate = unparsedUpdate;
        this.parsedUpdate = JsonParser.parseString(unparsedUpdate).getAsJsonArray();
    }
}
