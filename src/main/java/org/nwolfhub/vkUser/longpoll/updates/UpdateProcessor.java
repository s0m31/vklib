package org.nwolfhub.vkUser.longpoll.updates;

import com.google.gson.JsonParser;

import java.io.IOException;

public abstract class UpdateProcessor {
    public static Update convertUpdateByType(String unparsedUpdate) throws IOException { //old, il make it deprecated soon
        if(JsonParser.parseString(unparsedUpdate).getAsJsonArray().get(0).getAsInt()==4) {
            return new NewMessageUpdate(unparsedUpdate);
        }
        return new Update(unparsedUpdate, "nonetype");
    }
}
