package org.nwolfhub.vk.requests;

import java.io.Serializable;

public class DeleteMessage extends Request implements Serializable { //why am I even implementing serializable in every child class?
    public DeleteMessage(Integer peer_id, String message_ids) {
        super("messages.delete", "peer_id=" + peer_id, "message_ids=" + message_ids);
    }

    public DeleteMessage(Integer peer_id, String message_ids, boolean delete_for_all) {
        super("messages.delete", "peer_id=" + peer_id, "message_ids=" + message_ids, "delete_for_all=" + (delete_for_all?1:0));
    }
}
