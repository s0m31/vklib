package org.nwolfhub.vk.requests;

public class FriendsAdd extends Request {
    public FriendsAdd(Integer user_id) {
        super("friends.add", "user_id=" + user_id);
    }
}
