package org.nwolfhub.vk.requests;

public class GroupsIsMember extends Request {

    public GroupsIsMember(Integer group_id, Integer user_id) {
        super("groups.isMember", "group_id=" + group_id, "user_id=" + user_id);
    }
    public GroupsIsMember(Integer group_id, String user_ids) {
        super("groups.isMember", "group_id=" + group_id, "user_ids=" + user_ids);
    }

}
