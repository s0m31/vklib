package org.nwolfhub.vk.requests;

public class FriendsDelete extends Request {

    public FriendsDelete(Integer user_id) {
        super("friends.delete", "user_id=" + user_id);
    }
}
