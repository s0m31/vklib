package org.nwolfhub.vk.requests;

public class GroupsGetById extends Request{

    public GroupsGetById(Integer group_id) {
        super("groups.getById", "group_id=" + group_id);
    }
    public GroupsGetById(String group_ids) {
        super("groups.getById", "group_ids=" + group_ids);
    }
}
