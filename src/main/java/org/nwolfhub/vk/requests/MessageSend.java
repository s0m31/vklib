package org.nwolfhub.vk.requests;

import org.nwolfhub.vk.keyboard.Keyboard;

import java.io.Serializable;
import java.util.Random;

public class MessageSend extends Request implements Serializable {

    public MessageSend(Integer peer_id, String message) {
        super("messages.send", "peer_id=" + peer_id, "message=" + message, "random_id=" + new Random().nextInt());
    }

    public MessageSend(Integer peer_id, String message, String keyboard) {
        super("messages.send", "peer_id=" + peer_id, "message=" + message, "random_id=" + new Random().nextInt(), "keyboard=" + keyboard);
    }

    public MessageSend(Integer peer_id, String message, Keyboard keyboard) {
        super("messages.send", "peer_id=" + peer_id, "message=" + message, "random_id=" + new Random().nextInt(), "keyboard=" + keyboard.toString());
    }

}
