package org.nwolfhub.vk.requests;

import org.apache.http.client.utils.URIBuilder;
import org.nwolfhub.vkUser.Vk;

import java.io.Serializable;
import java.net.URISyntaxException;

public class Request implements Serializable {
    public final String method;
    public final String[] parameters;

    public Request(String method, String... params) {
        this.method = method;
        this.parameters = params;
    }

    public Request(String method) {
        this.method = method;
        this.parameters = null;
    }

    @Override
    public String toString() {
        try {
            URIBuilder builder = new URIBuilder("https://api.vk.com/method/" + method);
            if(parameters!=null) {
                for (String param : parameters) {
                    String[] holder = param.split("=");
                    builder.addParameter(holder[0], holder[1]);
                }
            }
            return builder.toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return "0";
    }
}
