package org.nwolfhub.vk.requests;

public class UsersGet extends Request {

    public UsersGet() {
        super("users.get");
    }

    public UsersGet(String user_ids) {
        super("users.get", "user_ids=" + user_ids);
    }

    public UsersGet(Integer user_id) {super("users.get", "user_ids=" + user_id);}

    public UsersGet(String user_ids, String fields) {
        super("users.get", "user_ids=" + user_ids, "fields=" + fields);
    }
    public UsersGet(Integer user_id, String name_case) {
        super("users.get", "user_ids=" + user_id, "name_case=" + name_case);
    }
    public UsersGet(String user_ids, String fields, String name_case) {
        super("users.get", "user_ids=" + user_ids, "fields=" + fields, "name_case=" + name_case);
    }

}
