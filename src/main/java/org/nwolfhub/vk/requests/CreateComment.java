package org.nwolfhub.vk.requests;

public class CreateComment extends Request {

    public CreateComment(Integer owner_id, Integer post_id, String message) {
        super("wall.createComment", "owner_id=" + owner_id, "post_id=" + post_id, "message=" + message);
    }
}
