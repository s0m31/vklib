package org.nwolfhub.vk.requests;

import java.io.Serializable;

public class EditMessage extends Request implements Serializable {

    public EditMessage(Integer peer_id, String message_id, String message) {
        super("messages.edit", "peer_id=" + peer_id, "message_id=" + message_id, "message=" + message);
    }
    public EditMessage(Integer peer_id, String message_id, String message, boolean keepForwarded) {
        super("messages.edit", "peer_id=" + peer_id, "message_id=" + message_id, "message=" + message, "keep_forward_messages=" + (keepForwarded?1:0));
    }
}
