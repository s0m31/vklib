package org.nwolfhub.vk.requests;

public class WallPost extends Request {

    public WallPost(Integer owner_id, String message) {
        super("wall.post", "owner_id=" + owner_id, "message=" + message);
    }
}
