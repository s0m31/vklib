# VKLib by nwolfhub   
Maven:   
```
<dependency>
  <groupId>org.nwolfhub.vk</groupId>
  <artifactId>vklib</artifactId>
  <version>1.1.1</version>
</dependency>
```
Gradle:
`implementation 'org.nwolfhub.vk:vklib:1.1.1'`
## ВКонтакте для сообществ   
### Инициализация
```java
VkGroup foo = new VkGroup("token", 12345/*group_id*/); //Клиент OkHttp сгенерируется при создании экземпляра
OkHttpClient client = new OkHttpClient();
VkGroup bar = new VkGroup("token", 12345, client); //или передайте свой
```
Учтите, что при передаче своего клиента на момент 1.0 нет проверки на валидность токена. Может, потом пофикшу.  
### Запросы к серверу
На момент 1.0 из заранее заготовленных запросов есть только messages.send:
```java
VkGroup vk = new VkGroup("token", group_id);
MessagesSend request = new MessagesSend(peer_id, "message");
vk.makeRequest(request);
```
Несмотря на это, можно составить любой запрос самостоятельно:
```java
Request request = new Request("stories.get", "owner_id=12345"); //название метода, аргументы через запятую.
//Аргументы передаются в виде название=значение
vk.makeRequest(request);

Request another_request = new Request("users.get", "user_ids=1", "fields=sex");
vk.makeRequest(another_request);
```
Если какой то метод вызывается часто, можно создать класс чисто для него:
```java
public static class UsersGet extends Request {
  public UsersGet(Integer id) {
    super("users.get", "user_id=" + id);
  }
}

public static void main(String[] args) throws IOException {
  VkGroup vk = new VkGroup("token", 12345);
  String response = vk.makeRequest(new UsersGet(1));
}
```
### LongPollинг
Для получения новых событий можно использовать LongPoll:
```java
VkGroup vk = new VkGroup("token", 12345);
LongPoll lp = vk.initLongPoll(); //Убедитесь, что импортите LongPoll из org.nwolfhub.vk.longpoll, а не org.nwolfhub.vkUser.longpoll
List<Update> updates = lp.getUpdates();
for (Update u:updates) {
  System.out.println(u.type);
}
```

Для получения событий о новом сообщении можно конвертировать их
```java
List<Update> updates = LongPoll.convertUpdateByType(Update.Type.message_new, lp.getUpdates());
for(Update update:updates) {
  NewMessageUpdate nu = (NewMessageUpdate) update;
  System.out.println(nu.message.getFrom_id() + ": " + nu.message.getText());
}
```
